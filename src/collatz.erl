-module(collatz).

-behaviour(gen_server).

-export([start_link/0,
    calculate/1,
    add_to_q/1]).

%% gen_server callbacks
-export([init/1,
         handle_call/3,
         handle_cast/2,
         handle_info/2,
         terminate/2,
         code_change/3]).

-record(state, {req_value, rec_replies, best_chain}).

-define(INIT_STATE, #state{req_value = 0, rec_replies = 0, best_chain = {0, 0}}).

start_link() ->
    gen_server:start_link({local, ?MODULE}, ?MODULE, [], []).

add_to_q(0)     -> ok;
add_to_q(N)     -> wpool:cast(target_pool, {N, self()}, best_worker),
                   add_to_q(N-1).

calculate(N) when (N>=0) -> 
            gen_server:cast(?MODULE, N).

init([]) ->
    Size = erlang:system_info(schedulers)*8,
    io:format( "Create pool of ~p workers~n",[Size]),
    wpool:start_pool(target_pool,[{workers, Size}, {worker, {worker_proc, []}}]),
    {ok, ?INIT_STATE}.

handle_call(_Request, _From, State) ->
    {reply, ignored, State}.

handle_cast(N, ?INIT_STATE)  ->
     add_to_q(N),
    {noreply, #state{req_value = N, rec_replies = 0, best_chain = {0,0}}};
handle_cast(_Msg, _State) ->
    {noreply, _State}.

handle_info({N,CLength}, #state{req_value = RV, rec_replies = RR, best_chain = {Number, Length}})  when (N>=0),
                                                                                                  (CLength>=0)->
    {NewN, NewL} = if 
                    CLength >= Length -> {N,CLength};
                    true -> {Number, Length}
                end,
    State = if 
              RR+1 =:=  RV ->  io:format(" Longest chain is ~p from ~p ~n",[NewL, NewN]),
                               ?INIT_STATE;
              true -> #state{req_value = RV, rec_replies = RR+1, best_chain = {NewN, NewL}}
    end, 
    {noreply, State};
handle_info(_Info, State) ->
    {noreply, State}.

terminate(_Reason, _State) ->
    ok.

code_change(_OldVsn, State, _Extra) ->
    {ok, State}.

