-module(worker_proc).

-behaviour(gen_server).

-export([start_link/0]).

%% gen_server callbacks
-export([init/1,
         handle_call/3,
         handle_cast/2,
         handle_info/2,
         terminate/2,
         code_change/3]).

-record(state, {}).

start_link() ->
    gen_server:start_link({local, ?MODULE}, ?MODULE, [], []).

sequence( X ) -> sequence(X, 1).

sequence(1, N) -> N;
sequence(X, N) when (X rem 2 =:= 0) -> sequence(X div 2, N+1);
sequence(X, N) when (X rem 2 =:= 1) -> sequence(X * 3 + 1, N+1).

init([]) ->
    {ok, #state{}}.

handle_call(_Request, _From, State) ->
    {reply, ok, State}.

handle_cast({Number, From}, State) when is_integer(Number), is_pid(From) ->
    Length = sequence( Number ),
    From ! {Number, Length},
    {noreply, State};
handle_cast(_M, State) ->
    {noreply, State}.

handle_info(_Info, State) ->
    {noreply, State}.

terminate(_Reason, _State) ->
    ok.

code_change(_OldVsn, State, _Extra) ->
    {ok, State}.

