REBAR = './rebar'

all: deps compile

deps:
	@( $(REBAR) get-deps )

compile: clean
	@( $(REBAR) compile )

clean:
	@( $(REBAR) clean )

run:
	@( erl +P 134217727 -pa ebin deps/*/ebin -s workers_pool )

.PHONY: all deps compile clean run

