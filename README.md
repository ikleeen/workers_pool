# Collatz problem 

Implementation is based on the inaka/worker_pool (https://github.com/inaka/worker_pool)  

### Build
-------------
Get dependencies:
```sh
    $ make deps
```
Compile app:
```sh
    $ make all
```
Run app:
```sh
    $ make run
```
### Run task
-------------
```sh
    $ collatz:calculate(1000000).
```
and wait result